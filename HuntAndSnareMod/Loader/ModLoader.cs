﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;

using KyuVulpes.HaSModLoader.Core;

using Newtonsoft.Json;

using UnityEngine;

namespace KyuVulpes.HaSModLoader.Loader {

	/// <summary>
	/// Manages mod loading and ordering.
	/// The ordering of when things happen is as follows:
	/// <list type="number">
	///		<item>
	///			<description>Assembly loaded</description>
	///		</item>
	///		<item>
	///			<description>Scan for all classes that extends the class <see cref="Mod"/>.</description>
	///		</item>
	///		<item>
	///			<description>Scan for, and calls the constructor of, classes that have the <see cref="StaticConstructorOnLoadAttribute"/>.</description>
	///		</item>
	///		<item>
	///			<description>Creates the <see cref="Mod"/> object.</description>
	///		</item>
	/// </list>
	/// All assemblies must be placed within a folder called `Assembly`.
	/// Textures are to be placed in the `Texture` folder and so on.
	/// See <see cref="ModInfo"/> for more about the structure of the folder.
	/// 
	/// The `About` folder is different, it contains a special file that holds data about the mod.
	/// There are 2 files that go into the `About` folder, and they are `info.json` and `hash.sha`.
	/// Info.json contains information about the mod. Such as:
	/// <list type="bullet">
	///		<item>
	///			<description>Mod Name</description>
	///		</item>
	///		<item>
	///			<description>Author</description>
	///		</item>
	///		<item>
	///			<description>Version Number</description>
	///		</item>
	///		<item>
	///			<description>Description</description>
	///		</item>
	///		<item>
	///			<description>Mod ID</description>
	///		</item>
	/// </list>
	/// Hash.sha should have the hash and the path on the same line seperated by a tab, the hash is first followed by the realitive path.
	/// The hashing algrithm to use is SHA-256, however, replacing `.sha` with `.md5` will make it use md5 hashing.
	/// If there is no `hash.sha` or `hash.md5`, the mod loader will load the mod even if it is corrupted or modified.
	/// Relating to the main assembly, it should start with `Intro.` to denote that it is the main assembly to load.
	/// </summary>
	public static class ModLoader {

		/// <summary>
		/// The location of where the mod folder is located at.
		/// </summary>
		public static readonly string MOD_FOLDER = Path.Combine( Application.dataPath, "..", "Mods" );

		/// <summary>
		/// If any mods have been loaded.
		/// </summary>
		public static bool ModsLoaded => mods.Count > 0;

		/// <summary>
		/// The number of mods loaded into the game.
		/// </summary>
		public static int ModCount => mods.Count;

		private static LinkedList<Mod> mods = new LinkedList<Mod>();

		static ModLoader() {
			ExtendedLogger.Message( "Setting things up before continuing." );

			if ( !Directory.Exists( MOD_FOLDER ) ) {
				_ = Directory.CreateDirectory( MOD_FOLDER );
			}

			AppDomain.CurrentDomain.AssemblyResolve += ( _, args ) => ModAssemblyResolver( args );

			// Used to load assemblies on the fly when a mod requests it.
			Assembly ModAssemblyResolver( ResolveEventArgs args ) {
				ExtendedLogger.Message( $"Looking to resolve missing assembly with the name `{args.Name}`, which has been requested by `{args.RequestingAssembly.GetName().Name}`." );
				foreach ( var modFolder in Directory.EnumerateDirectories( MOD_FOLDER, "*", SearchOption.TopDirectoryOnly ) ) {
					foreach ( var asm in Directory.EnumerateFiles( Path.Combine( modFolder, "Assembly" ), "*.dll", SearchOption.TopDirectoryOnly ) ) {
						var asmName = AssemblyName.GetAssemblyName( asm );

						if ( args.Name.Equals( asmName.Name, StringComparison.OrdinalIgnoreCase ) ) {
							ExtendedLogger.Message( $"Found the assembly at `{asm}`." );

							return Assembly.LoadFrom( asm );
						}
					}
				}

				ExtendedLogger.Warning( "Failed to find unloaded assembly!" );

				return null;
			}
		}

		/// <summary>
		/// Enables find of a mod based on the mod type.
		/// </summary>
		/// <param name="modType">The type of mod to get from the database.</param>
		/// <returns><see langword="null"/> if it has not been loaded, otherwise the mod object itself.</returns>
		public static Mod FindModInDatabase( Type modType ) => mods.Where( m => m.GetType() == modType ).FirstOrDefault();

		/// <summary>
		/// Enables iterating through the entire database.
		/// </summary>
		/// <returns>The database <see cref="IEnumerator{T}"/>.</returns>
		public static IEnumerator<Mod> GetEnumerator() => mods.GetEnumerator();

		internal static void BeginModLoading() {
			foreach ( var modFolder in Directory.GetDirectories( MOD_FOLDER, "*", SearchOption.TopDirectoryOnly ) ) {
				ExtendedLogger.Message( $"Loading mod in `{modFolder}`." );

				if ( !Directory.Exists( Path.Combine( modFolder, "About" ) ) ) {
					ExtendedLogger.Message( "Skipping." );

					continue;
				}

				var about = default( ModInfo.About );

				try {
					var json = File.ReadAllText( Path.Combine( modFolder, "About", "info.json" ) );

					about = JsonConvert.DeserializeObject<ModInfo.About>( json );
				} catch ( Exception e ) {
					ExtendedLogger.Error( e );
				}

				var info = new ModInfo( about, modFolder );
				var hashSuccess = true;

				// Checks to validate that a hash file exists and rather it is sha or md5.
				// It will then call a function to verify it.
				if ( File.Exists( Path.Combine( modFolder, "About", "hash.sha" ) ) ) {
					var backupCwd = Environment.CurrentDirectory;

					ExtendedLogger.Message( $"Changing current working directory to `{modFolder}` and verifying hash." );

					Environment.CurrentDirectory = modFolder;

					hashSuccess = ValidateHashFile( Path.Combine( modFolder, "About", "hash.sha" ) );

					Environment.CurrentDirectory = backupCwd;
				} else if ( File.Exists( Path.Combine( modFolder, "About", "hash.md5" ) ) ) {
					var backupCwd = Environment.CurrentDirectory;

					ExtendedLogger.Message( $"Changing current working directory to `{modFolder}` and verifying hash." );

					Environment.CurrentDirectory = modFolder;

					hashSuccess = ValidateHashFile( Path.Combine( modFolder, "About", "hash.md5" ) );

					Environment.CurrentDirectory = backupCwd;
				} else {
					ExtendedLogger.Warning( "Please ensure that the mod came from a trusted source, using mods can be dangerous.\nThis is why the SHA256 and MD5 checks are in place, to ensure no comprimises." );
				}

				// If the checks failed, then the libraries shouldn't be running. Plain and simple as that.
				if ( !hashSuccess ) {
					ExtendedLogger.Message( "Hash verification failed, moving to next mod." );

					continue;
				}

				ExtendedLogger.Message( "Either hash was checked successfully, or no hash file found. Continuing to load mod." );

				// If it failed to load the assembly, it will skip the rest of the mod and move on to the next.
				if ( !LoadAssemblies( Path.Combine( modFolder, "Assembly" ), out var modAsm ) ) {
					ExtendedLogger.Error( "Failed to locate the starting assembly, moving to next mod." );

					continue;
				}

				Type modType = default;

				// Grabbing the mod class of the assembly.
				foreach ( var type in modAsm.GetTypes() ) {
					if ( typeof( Mod ).IsAssignableFrom( type ) && modType is null ) {
						modType = type;

						ExtendedLogger.Message( "Found the mod class, saving for later creation." );
					} else if ( type.GetCustomAttribute<StaticConstructorOnLoadAttribute>() is StaticConstructorOnLoadAttribute ) {
						ExtendedLogger.Message( $"Invoking static constructor for `{type.FullName}`." );

						try {
							RuntimeHelpers.RunClassConstructor( type.TypeHandle );
						} catch ( Exception e ) {
							ExtendedLogger.Error( e );
						}
					}
				}

				if ( modType is Type ) {
					var mod = ( Mod )Activator.CreateInstance( modType, new[] { info } );

					mods.AddLast( mod );

					ExtendedLogger.Message( $"Mod `{info.AboutMod.ModName}` has been created and added to the database." );
				}
			}
		}

		private static bool LoadAssemblies( string asmFolderPath, out Assembly modAsm ) {
			var asms = Directory.GetFiles( asmFolderPath, "*.dll", SearchOption.TopDirectoryOnly );

			modAsm = null;

			foreach ( var asmFile in asms ) {
				var asm = Assembly.LoadFrom( asmFile );

				ExtendedLogger.Message( $"Loaded `{asm.GetName().Name}`." );

				if ( modAsm is null && asm.GetTypes().FirstOrDefault( t => typeof( Mod ).IsAssignableFrom( t ) ) is Type ) {
					modAsm = asm;
				}
			}

			return true;
		}

		private static bool ValidateHashFile( string hashFile ) {
			using ( HashAlgorithm hash = hashFile.EndsWith( ".sha" ) ? ( HashAlgorithm )SHA256.Create() : MD5.Create() ) {
				var hashLines = File.ReadAllText( hashFile ).Split( '\n' );

				for ( var i = 0; i < hashLines.Length; ++i ) {
					var split = hashLines[i].Split( '\t' );

					ExtendedLogger.Message( $"Verifying hash for `{split[1]}`." );

					if ( !File.Exists( split[1] ) ) {
						ExtendedLogger.Error( "File does not exist." );

						return false;
					}

					using ( var file = new FileStream( split[1], FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
						var computeHash = BitConverter.ToString( hash.ComputeHash( file ) ).Replace( "-", string.Empty );

						if ( !computeHash.Equals( split[0], StringComparison.OrdinalIgnoreCase ) ) {
							ExtendedLogger.Error( $"Hash did not match, expected `{split[0]}` but got `{computeHash}`." );

							return false;
						}

						ExtendedLogger.Message( "Hash matched, moving to next file within the hash file." );
					}
				}
			}

			return true;
		}

	}
}
