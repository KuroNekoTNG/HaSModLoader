﻿using System;

namespace KyuVulpes.HaSModLoader.Loader {
	/// <summary>
	/// This <see cref="Attribute"/> is meant to run static constructors upon loading up the mod.
	/// This is meant to be used to initialize things that are required to be on the main thread.
	/// Only one of these attributes can be applied to a class.
	/// </summary>
	/// <remarks>
	/// For best result, do NOT put these in classes outside the mod's main assembly.
	/// This is because only the mods main assembly is checked.
	/// </remarks>
	[AttributeUsage( AttributeTargets.Class, AllowMultiple = false, Inherited = false )]
	public sealed class StaticConstructorOnLoadAttribute : Attribute { }
}
