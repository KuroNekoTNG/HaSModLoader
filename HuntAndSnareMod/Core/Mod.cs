﻿using System;

namespace KyuVulpes.HaSModLoader.Core {

	/// <summary>
	/// The base class for all mods.
	/// This class also contains methods that should prove useful when using settings.
	/// </summary>
	public abstract class Mod {

		/// <summary>
		/// The ID/Package ID of the mod.
		/// </summary>
		public Guid ID => Info.AboutMod.PackageID;

		/// <summary>
		/// Easy access to things that are probably needed.
		/// </summary>
		/// <seealso cref="ModInfo"/>
		public ModInfo Info {
			get;
		}

		/// <summary>
		/// The base constructor which is used for pre-setup of the mod.
		/// Please ensure that it is called when extending this class.
		/// </summary>
		/// <param name="info">The information passed to the mod. See <see cref="ModInfo"/> for more information.</param>
		public Mod( ModInfo info ) {
			Info = info;
		}

		/// <summary>
		/// A shortcut to the <see cref="ModSettingManager.AddSetting(Guid, string, object)"/>.
		/// </summary>
		/// <param name="key">The key to the value.</param>
		/// <param name="value">The value to add.</param>
		public void AddSetting( string key, object value ) => ModSettingManager.AddSetting( ID, key, value );

		/// <summary>
		/// A shortcut to the <see cref="ModSettingManager.TryAddSetting(Guid, string, object)"/>.
		/// </summary>
		/// <param name="key">The key to the value.</param>
		/// <param name="value">The value to add.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public bool TryAddSetting( string key, object value ) => ModSettingManager.TryAddSetting( ID, key, value );

		/// <summary>
		/// A shortcut to the <see cref="ModSettingManager.SetSetting(Guid, string, object)"/>.
		/// </summary>
		/// <param name="key">The key to the value.</param>
		/// <param name="value">The value to set to the key.</param>
		public void SetSetting( string key, object value ) => ModSettingManager.SetSetting( ID, key, value );

		/// <summary>
		/// A shortcut to the <see cref="ModSettingManager.TrySetSetting(Guid, string, object)"/>.
		/// </summary>
		/// <param name="key">The key to the value.</param>
		/// <param name="value">The value to set to the key.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public bool TrySetSetting( string key, object value ) => ModSettingManager.TrySetSetting( ID, key, value );

		/// <summary>
		/// A shortcut to <see cref="ModSettingManager.GetSetting(Guid, string)"/>.
		/// </summary>
		/// <param name="key">The key to access this value.</param>
		/// <returns>A generic <see cref="object"/> that is associated with the key.</returns>
		public object GetSetting( string key ) => ModSettingManager.GetSetting( ID, key );

		/// <summary>
		/// A shortcut to <see cref="ModSettingManager.TryGetSetting(Guid, string, out object)"/>.
		/// </summary>
		/// <param name="key">The key to access the value.</param>
		/// <param name="value">The <see cref="object"/> associated with the <paramref name="key"/>.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public bool TryGetSetting( string key, out object value ) => ModSettingManager.TryGetSetting( ID, key, out value );

		/// <summary>
		/// A shortcut to <see cref="ModSettingManager.GetSetting{T}(Guid, string)"/>.
		/// </summary>
		/// <typeparam name="T">The type to auto-convert to.</typeparam>
		/// <param name="key">The key to access the value.</param>
		/// <returns>An object that was converted to <typeparamref name="T"/>.</returns>
		public T GetSetting<T>( string key ) => ModSettingManager.GetSetting<T>( ID, key );

		/// <summary>
		/// A shortcut to <see cref="ModSettingManager.TryGetSetting{T}(Guid, string, out T)"/>.
		/// </summary>
		/// <typeparam name="T">The type to auto-convert to.</typeparam>
		/// <param name="key">The key to access the value.</param>
		/// <param name="value">An object that was converted to <typeparamref name="T"/>.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		/// <remarks>
		/// <paramref name="value"/> is the default for that type if it failed to get the value and/or convert it.
		/// </remarks>
		public bool TryGetSetting<T>( string key, out T value ) => ModSettingManager.TryGetSetting( ID, key, out value );
	}
}
