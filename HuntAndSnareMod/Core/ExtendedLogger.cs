﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

using UnityEngine;

namespace KyuVulpes.HaSModLoader.Core {
	/// <summary>
	/// A class for you modders to use that will aid in modding.
	/// Note that while you could use <see cref="Debug"/> to log to, this class does that and also color codes output along with sending them to <see cref="Debug"/>.
	/// </summary>
	public static class ExtendedLogger {

		private const string KERNEL32_DLL_NAME = "kernel32.dll";

		static ExtendedLogger() {
			if ( SystemInfo.operatingSystemFamily == OperatingSystemFamily.Windows ) {
				Show();
			}

			Console.SetCursorPosition( 0, 0 );
		}

		private static bool HasConsole {
			get {
				return GetConsoleWindow() != IntPtr.Zero;
			}
		}

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		[return: MarshalAs( UnmanagedType.Bool )]
		private static extern bool AllocConsole();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern bool FreeConsole();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern IntPtr GetConsoleWindow();

		[DllImport( KERNEL32_DLL_NAME, SetLastError = true )]
		private static extern int GetConsoleOutputCp();

		/// <summary>
		/// Prints the message to the console if one exists, and to the Unity logs.
		/// </summary>
		/// <param name="message">The message to print.</param>
		public static void Message( string message ) {
			if ( HasConsole ) {
				Console.WriteLine( message );
			}

			Debug.Log( message );
		}

		/// <summary>
		/// Prints a warning message to the console if one exists, and to the Unity logs.
		/// </summary>
		/// <param name="message">The warning to print out.</param>
		/// <param name="e">An option exception if one is avialable.</param>
		public static void Warning( string message, Exception e = null ) {
			if ( HasConsole ) {
				Console.ForegroundColor = ConsoleColor.Yellow;

				Console.Error.WriteLine( message );

				try {
					var exMessage = GenerateExceptionMessage( e );
					Console.Error.WriteLine( exMessage );
				} catch {
					Console.Error.WriteLine( "No exception was generated." );
				}

				Console.ResetColor();
			}

			Debug.LogWarning( message );
		}

		/// <summary>
		/// Prints out an error to the console if one exists, and also to the Unity logs.
		/// </summary>
		/// <param name="message">The error message to print out.</param>
		/// <remarks>If you wish to print out an exception, please use <see cref="Error(Exception)"/>.</remarks>
		public static void Error( string message ) {
			if ( HasConsole ) {
				Console.ForegroundColor = ConsoleColor.Red;

				Console.Error.WriteLine( message );

				Console.ResetColor();
			}

			Debug.LogError( message );
		}

		/// <summary>
		/// Prints out an error using <paramref name="e"/>.
		/// </summary>
		/// <param name="e">The <see cref="Exception"/> that caused the error.</param>
		public static void Error( Exception e ) => Error( GenerateExceptionMessage( e ) );

		private static string GenerateExceptionMessage( Exception e ) {
			if ( e is null ) {
				throw new ArgumentNullException( nameof( e ), "An exception must be passed." );
			}

			var ex = e;
			var builder = new StringBuilder();

			while ( ex is Exception ) {
				builder.AppendLine( $"Exception:\t{e.GetType().FullName}\nMessage:\t{e.Message}\nStacktrace:\n{e.StackTrace}" );

				if ( ex.InnerException is Exception ) {
					builder.Append( '-', 15 ).Append( "Inner Exception" ).Append( '-', 15 ).AppendLine();
				}

				ex = ex.InnerException;
			}

			return builder.ToString().Trim();
		}

		private static void Show() {
			if ( !HasConsole ) {
				AllocConsole();
				InvalidateOutAndError();
			}
		}

		private static void InvalidateOutAndError() {
			var typeFromHandle = typeof( Console );
			var _out = typeFromHandle.GetField( "stdout", BindingFlags.Static | BindingFlags.NonPublic );
			var _err = typeFromHandle.GetField( "stderr", BindingFlags.Static | BindingFlags.NonPublic );
			var constructor = typeFromHandle.GetConstructor( BindingFlags.Static | BindingFlags.NonPublic, null, Array.Empty<Type>(), null );

			_out.SetValue( null, null );
			_err.SetValue( null, null );

			constructor.Invoke( null, Array.Empty<object>() );
		}

	}
}
