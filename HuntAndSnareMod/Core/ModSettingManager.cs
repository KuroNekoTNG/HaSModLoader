﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using UnityEngine;

namespace KyuVulpes.HaSModLoader.Core {

	/// <summary>
	/// Manages the settings for any mod that uses it.
	/// </summary>
	public static class ModSettingManager {

		/// <summary>
		/// The location of where the settings are stored.
		/// It uses the <see cref="Application.persistentDataPath"/> that Unity provides but also creates a sub-folder where everything resides in.
		/// </summary>
		public static readonly string SETTING_LOCATION = Path.Combine( Application.persistentDataPath, "Kyu Vulpes", "Mod Loader", "Settings" );

		private static Dictionary<Guid, Dictionary<string, object>> modSettings;

		static ModSettingManager() {
			ExtendedLogger.Message( $"All settings can be found at `{SETTING_LOCATION}`." );

			if ( !Directory.Exists( SETTING_LOCATION ) ) {
				_ = Directory.CreateDirectory( SETTING_LOCATION );

				modSettings = new Dictionary<Guid, Dictionary<string, object>>();

				return;
			}

			var settingFiles = Directory.GetFiles( SETTING_LOCATION, "*.bin", SearchOption.TopDirectoryOnly );

			for ( var i = 0; i < settingFiles.Length; ++i ) {
				using ( var settingFile = new FileStream( settingFiles[i], FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
					var guidStr = new FileInfo( settingFiles[i] ).Name.Replace( ".bin", "" );
					var guid = Guid.Parse( guidStr );
					var dic = PopulateSettingDictionary( settingFile );

					if ( dic is null ) {
						continue;
					}

					modSettings.Add( guid, dic );
				}
			}

			Application.quitting += SaveSettings;

			Dictionary<string, object> PopulateSettingDictionary( Stream settingFile ) {
				var setting = new Dictionary<string, object>();

				using ( var binReader = new BinaryReader( settingFile, Encoding.Unicode, true ) ) {
					var count = binReader.ReadInt16();

					for ( var i = 0; i < count; ++i ) {
						var key = binReader.ReadString();
						var typeStr = binReader.ReadString();
						var type = Type.GetType(
							typeStr,
							AssemblyResolver,
							TypeResolver
						);
						var hasChildren = binReader.ReadBoolean();
						object obj;

						if ( type is null ) {
							return null;
						}

						obj = ParseType( type, binReader );

						setting.Add( key, obj );
					}
				}

				return setting;

				Assembly AssemblyResolver( AssemblyName asmName ) => AppDomain.CurrentDomain.GetAssemblies().Where( asm => asm.GetName() == asmName ).FirstOrDefault();
				Type TypeResolver( Assembly asm, string typeName, bool caseSens ) => asm.GetTypes()
					.Where( t => t.Name.Equals( typeName, caseSens ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal ) )
					.FirstOrDefault();
				object ParseType( Type type, BinaryReader binReader ) {
					if ( type == typeof( byte ) || type == typeof( sbyte ) || type == typeof( bool ) ) {
						return binReader.ReadByte();
					} else if ( type == typeof( short ) || type == typeof( ushort ) || type == typeof( char ) ) {
						return binReader.ReadInt16();
					} else if ( type == typeof( int ) || type == typeof( uint ) || type == typeof( float ) ) {
						return binReader.ReadInt32();
					} else if ( type == typeof( long ) || type == typeof( ulong ) || type == typeof( double ) ) {
						return binReader.ReadInt64();
					} else if ( type == typeof( decimal ) ) {
						return binReader.ReadDecimal();
					} else if ( type == typeof( string ) ) {
						return binReader.ReadString();
					} else if ( type.IsEnum ) {
						var val = ParseType( Enum.GetUnderlyingType( type ), binReader );

						return Convert.ChangeType( val, type );
					} else if ( type == typeof( Vector2 ) ) {
						var x = binReader.ReadSingle();
						var y = binReader.ReadSingle();

						return new Vector2( x, y );
					} else if ( type == typeof( Vector2Int ) ) {
						var x = binReader.ReadInt32();
						var y = binReader.ReadInt32();

						return new Vector2Int( x, y );
					} else if ( type == typeof( Vector3 ) ) {
						var x = binReader.ReadSingle();
						var y = binReader.ReadSingle();
						var z = binReader.ReadSingle();

						return new Vector3( x, y, z );
					} else if ( type == typeof( Vector3Int ) ) {
						var x = binReader.ReadInt32();
						var y = binReader.ReadInt32();
						var z = binReader.ReadInt32();

						return new Vector3Int( x, y, z );
					} else if ( type == typeof( Vector4 ) ) {
						var x = binReader.ReadSingle();
						var y = binReader.ReadSingle();
						var z = binReader.ReadSingle();
						var w = binReader.ReadSingle();

						return new Vector4( x, y, z, w );
					} else if ( type == typeof( List<> ) ) {
						var count = binReader.ReadInt32();
						var items = new object[count];
						var list = Activator.CreateInstance( type );
						var itemList = type.GetField( "_items", BindingFlags.Instance | BindingFlags.NonPublic );

						for ( var i = 0; i < count; ++i ) {
							items[i] = ParseType( type.GetGenericArguments()[0], binReader );
						}

						itemList.SetValue( list, items );

						return itemList;
					} else {
						throw new Exception();
					}
				}
			}
		}

		/// <summary>
		/// Adds the settings to the dictionary upon request.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The key to pair with the <paramref name="value"/>.</param>
		/// <param name="value">The value to store.</param>
		/// <exception cref="ArgumentException">
		/// Either the <paramref name="value"/>'s type is not an <see cref="Enum"/> or a primitive. Could also be from the key already being in the settings.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// The key is null.
		/// </exception>
		/// <seealso cref="Dictionary{TKey, TValue}.Add(TKey, TValue)"/>
		/// <remarks>
		/// For a better way to add to the settings, try using <see cref="TryAddSetting(Guid, string, object)"/>.
		/// </remarks>
		public static void AddSetting( Guid id, string key, object value ) {
			var valType = value.GetType();

			if ( !( valType.IsPrimitive || valType.IsEnum ) ) {
				throw new ArgumentException( "Expected primitive or enum type.", nameof( value ) );
			}

			if ( !modSettings.ContainsKey( id ) ) {
				modSettings.Add( id, new Dictionary<string, object>() );
			}

			modSettings[id].Add( key, value );
		}

		/// <summary>
		/// Attempts to add the setting to the dictionary upon requests.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The key to pair with the <paramref name="value"/>.</param>
		/// <param name="value">The value to store.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public static bool TryAddSetting( Guid id, string key, object value ) {
			try {
				AddSetting( id, key, value );
				return true;
			} catch {
				return false;
			}
		}

		/// <summary>
		/// Changes the <paramref name="key"/> association to the new <paramref name="value"/>.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The key to pair with the <paramref name="value"/>.</param>
		/// <param name="value">The value to store.</param>
		/// <exception cref="ArgumentNullException">The key is null.</exception>
		/// <exception cref="KeyNotFoundException">The key does not exist within settings.</exception>
		/// <remarks>See <see cref="Dictionary{TKey, TValue}"/> for more detail. For a better way, use <see cref="TrySetSetting(Guid, string, object)"/>.</remarks>
		public static void SetSetting( Guid id, string key, object value ) {
			var valType = value.GetType();

			if ( !( valType.IsPrimitive || valType.IsEnum ) ) {
				throw new ArgumentException( "Expected primitive or enum type.", nameof( value ) );
			}

			modSettings[id][key] = value;
		}

		/// <summary>
		/// Attempts to change the <paramref name="key"/> association to the new <paramref name="value"/>.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The key to pair with the <paramref name="value"/>.</param>
		/// <param name="value">The value to store.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public static bool TrySetSetting( Guid id, string key, object value ) {
			try {
				SetSetting( id, key, value );
				return true;
			} catch {
				return false;
			}
		}

		/// <summary>
		/// Grabs the value from settings.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The <see cref="string"/> associated with the value.</param>
		/// <returns>The value associated with the <paramref name="key"/>.</returns>
		public static object GetSetting( Guid id, string key ) => modSettings[id][key];

		/// <summary>
		/// Grabs the value from settings and attempts to convert it to the requested type.
		/// </summary>
		/// <typeparam name="T">The type to convert it to.</typeparam>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The <see cref="string"/> associated with the value.</param>
		/// <returns>The value associated with the <paramref name="key"/>.</returns>
		public static T GetSetting<T>( Guid id, string key ) {
			var obj = modSettings[id][key];
			T val = ( T )Convert.ChangeType( obj, typeof( T ) );

			return val;
		}

		/// <summary>
		/// Attempts to grab the value from settings.
		/// </summary>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The <see cref="string"/> associated with the value.</param>
		/// <param name="obj">The value that is associated with the <paramref name="key"/> if successful, otherwise the default value.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public static bool TryGetSetting( Guid id, string key, out object obj ) {
			try {
				obj = GetSetting( id, key );
				return true;
			} catch {
				obj = null;
				return false;
			}
		}

		/// <summary>
		/// Attempts to grab the value from settings and converts it to the requested type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T">The type to convert it to.</typeparam>
		/// <param name="id">The <see cref="Guid"/> of the mod, can be found at <see cref="Mod.ID"/>.</param>
		/// <param name="key">The <see cref="string"/> associated with the value.</param>
		/// <param name="value">The value associated with the key if one exists.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public static bool TryGetSetting<T>( Guid id, string key, out T value ) {
			try {
				value = GetSetting<T>( id, key );
				return true;
			} catch {
				value = default;
				return false;
			}
		}

		/// <summary>
		/// This method checks to ensure that the type is valid for settings.
		/// </summary>
		/// <param name="type">The type to check.</param>
		/// <returns><see langword="true"/> if it is a valid type to save, otherwise <see langword="false"/>.</returns>
		public static bool IsValidSettingType( Type type ) {
			if ( IsKnownType( type ) ) {
				return true;
			}

			if ( type == typeof( List<> ) ) {
				return IsKnownType( type.GetGenericArguments()[0] );
			}

			return false;

			bool IsKnownType( Type t ) => t.IsPrimitive || t.IsEnum || t == typeof( Vector2 ) || t == typeof( Vector2Int )
			|| t == typeof( Vector3 ) || t == typeof( Vector3Int ) || t == typeof( Vector4 );
		}

		private static void SaveSettings() {
			foreach ( var pair in modSettings ) {
				using ( var settingStream = new FileStream( Path.Combine( SETTING_LOCATION, $"{pair.Key}.bin" ), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read ) ) {
					using ( var binWriter = new BinaryWriter( settingStream ) ) {
						SaveSetting( binWriter, pair.Value );
					}
				}
			}

			void SaveSetting( BinaryWriter bWriter, Dictionary<string, object> settings ) {
				bWriter.Write( settings.Count );

				foreach ( var pair in settings ) {
					var type = pair.Value.GetType();

					bWriter.Write( pair.Key );
					bWriter.Write( type.FullName );

					WriteType( type, pair.Value );
				}

				void WriteType( Type type, object value ) {
					if ( type == typeof( bool ) || type == typeof( byte ) || type == typeof( sbyte ) ) {
						bWriter.Write( Convert.ToByte( value ) );
					} else if ( type == typeof( short ) || type == typeof( ushort ) ) {
						bWriter.Write( Convert.ToInt16( value ) );
					} else if ( type == typeof( int ) || type == typeof( uint ) ) {
						bWriter.Write( Convert.ToInt32( value ) );
					} else if ( type == typeof( float ) ) {
						bWriter.Write( Convert.ToSingle( value ) );
					} else if ( type == typeof( long ) || type == typeof( ulong ) ) {
						bWriter.Write( Convert.ToInt64( value ) );
					} else if ( type == typeof( double ) ) {
						bWriter.Write( Convert.ToDouble( value ) );
					} else if ( type == typeof( decimal ) ) {
						bWriter.Write( Convert.ToDecimal( value ) );
					} else if ( type == typeof( string ) ) {
						bWriter.Write( ( string )value );
					} else if ( type.IsEnum ) {
						var baseType = Enum.GetUnderlyingType( type );

						WriteType( baseType, value );
					} else if ( type == typeof( Vector2 ) ) {
						bWriter.Write( ( ( Vector2 )value ).x );
						bWriter.Write( ( ( Vector2 )value ).y );
					} else if ( type == typeof( Vector2Int ) ) {
						bWriter.Write( ( ( Vector2Int )value ).x );
						bWriter.Write( ( ( Vector2Int )value ).y );
					} else if ( type == typeof( Vector3 ) ) {
						bWriter.Write( ( ( Vector3 )value ).x );
						bWriter.Write( ( ( Vector3 )value ).y );
						bWriter.Write( ( ( Vector3 )value ).z );
					} else if ( type == typeof( Vector3Int ) ) {
						bWriter.Write( ( ( Vector3Int )value ).x );
						bWriter.Write( ( ( Vector3Int )value ).y );
						bWriter.Write( ( ( Vector3Int )value ).z );
					} else if ( type == typeof( Vector4 ) ) {
						bWriter.Write( ( ( Vector4 )value ).x );
						bWriter.Write( ( ( Vector4 )value ).y );
						bWriter.Write( ( ( Vector4 )value ).z );
						bWriter.Write( ( ( Vector4 )value ).w );
					} else if ( type == typeof( List<> ) && IsValidSettingType( type.GetGenericArguments()[0] ) ) {
						var t = type.GetGenericArguments()[0];
						var items = ( object[] )type.GetField( "_items", BindingFlags.NonPublic | BindingFlags.Instance ).GetValue( value );

						bWriter.Write( items.Length );

						foreach ( var item in items ) {
							WriteType( t, item );
						}
					}
				}
			}
		}
	}
}
