﻿using System;
using System.IO;

using Newtonsoft.Json;

namespace KyuVulpes.HaSModLoader.Core {
	/// <summary>
	/// This class holds data for the mod to use.
	/// Note that this class might be expanded apon later on.
	/// </summary>
	public sealed class ModInfo {

		/// <summary>
		/// Information from the `Info.json` file in the `ModFolder/About` folder.
		/// </summary>
		public struct About {

			/// <summary>
			/// The version of the mod.
			/// </summary>
			public struct Version {

				/// <summary>
				/// The major release change to the mod.
				/// </summary>
				[JsonProperty( "major" )]
				public byte Major {
					get;
					private set;
				}

				/// <summary>
				/// The minor release change to the mod.
				/// </summary>
				[JsonProperty( "minor" )]
				public byte Minor {
					get;
					private set;
				}

				/// <summary>
				/// The patch number to the mod.
				/// </summary>
				[JsonProperty( "patch" )]
				public byte Patch {
					get;
					private set;
				}

				internal Version( byte major, byte minor, byte patch ) {
					Major = major;
					Minor = minor;
					Patch = patch;
				}

			}

			/// <summary>
			/// The name of the mod.
			/// </summary>
			[JsonProperty( "name" )]
			public string ModName {
				get;
				private set;
			}

			/// <summary>
			/// The person who created/programmed the mod.
			/// </summary>
			[JsonProperty( "author" )]
			public string Author {
				get;
				private set;
			}

			/// <summary>
			/// The description of the mod.
			/// </summary>
			[JsonProperty( "desc" )]
			public string Description {
				get;
				private set;
			}

			/// <summary>
			/// The package ID, using <see cref="Guid"/>, to ensure no mod overlap.
			/// </summary>
			[JsonProperty( "packageID" )]
			public Guid PackageID {
				get;
				private set;
			}

			/// <summary>
			/// The version of the mod.
			/// <see cref="Version"/>
			/// </summary>
			[JsonProperty( "version" )]
			public Version ModVersion {
				get;
				private set;
			}

			internal About( Guid packageId, Version version, string modName, string author, string desc ) {
				PackageID = packageId;
				ModVersion = version;
				ModName = modName;
				Author = author;
				Description = desc;
			}
		}

		/// <summary>
		/// The root of the mod's folder.
		/// Useful for when the mod needs to load data.
		/// </summary>
		public string ModRoot {
			get;
		}

		/// <summary>
		/// A struct that represents the info contained within the info.xml file.
		/// </summary>
		public About AboutMod {
			get;
		}

		/// <summary>
		/// A quick way to get to the assembly folder.
		/// </summary>
		public string Assembly => Path.Combine( ModRoot, "Assembly" );

		/// <summary>
		/// A quick way to get to the AssetBundle folder.
		/// </summary>
		public string AssetBundles => Path.Combine( ModRoot, "AssetBundle" );

		/// <summary>
		/// A quick way to get to the Texture folder.
		/// </summary>
		public string Textures => Path.Combine( ModRoot, "Texture" );

		/// <summary>
		/// A quick way to get to the Audio folder.
		/// </summary>
		public string Audio => Path.Combine( ModRoot, "Audio" );

		internal ModInfo( About about, string modRoot ) {
			AboutMod = about;
			ModRoot = modRoot;
		}

	}
}
