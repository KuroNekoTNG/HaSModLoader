﻿# (Hopefully) Community Driven Modding Documentation

These articles are here to help those wanting to mod the game of Hunt and Snare.
With these sets of articles, and help the others in the future, we can add content to this game between the long updates.
Decompile the game, poke and prode it, and report back with your finds on how to use it.
I am always welcome of new information. To ensure that what you know is true, please write a simple mod that uses that.

Of course, I will say that you are NOT allowed to post source code (even if it is the decompiled version) here.
Due to legal reasons, of course, and I feel like I should state that before anyone does such a thing.

Another side note is that this game is based of Unity 2018.4, so be sure to use those [docs](https://docs.unity3d.com/2018.4/Documentation/ScriptReference/index.html).
