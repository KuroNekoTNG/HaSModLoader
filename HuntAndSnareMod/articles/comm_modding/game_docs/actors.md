﻿# Actors

Actors appear to be any fur within the game. This means that you are an actor, your crew are actors, those you hunt are actors, and the NPCs you talk to are actors.
This means that changing the behaviour of this class through Harmony affects every one of those objects stated above.
This much information is about how much I have gathered from it, and more testing and proding of Actors is needed to fully understand it.

Actors extend `MonoBehaviour`, which means that they also have things that are part of the [`MonoBehaviour`](https://docs.unity3d.com/2018.4/Documentation/ScriptReference/MonoBehaviour.html) class.