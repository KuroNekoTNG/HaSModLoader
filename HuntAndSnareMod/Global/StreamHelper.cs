﻿using System;
using System.IO;

/// <summary>
/// This class is made for better usage of <see cref="Stream"/> classes.
/// I, Kyu Vulpes, find some things to be just dumb, like needing to pass extra data when all the data is there.
/// </summary>
public static class StreamHelper {

	/// <summary>
	/// Writes an entire byte[] to the stream.
	/// </summary>
	/// <param name="s">The stream to write to.</param>
	/// <param name="buffer">The array that contains the data.</param>
	public static void Write( this Stream s, byte[] buffer ) => s.Write( buffer, 0, buffer.Length );

	/// <summary>
	/// Reads the section of the stream to the buffer.
	/// If the buffer is to big, it will be shrinked down to size.
	/// </summary>
	/// <param name="s">The stream to read from.</param>
	/// <param name="buffer">The buffer to write out to.</param>
	public static void Read( this Stream s, ref byte[] buffer ) {
		var innerBuffer = buffer is null || buffer.Length == 0 ? new byte[s.Length] : new byte[buffer.Length];
		var count = s.Read( innerBuffer, 0, innerBuffer.Length );

		if ( count < innerBuffer.Length ) {
			buffer = new byte[count];

			Array.Copy( innerBuffer, buffer, count );
		}

		buffer = innerBuffer;
	}

}