﻿using System;
using System.Diagnostics;
using System.Text;
using KyuVulpes.HaSModLoader.Core;
using KyuVulpes.HaSModLoader.Loader;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

namespace KyuVulpes.HaSModLoader {
	/// <summary>
	/// This class is the entry point into the mod loader.
	/// Note that there is really nothing of importance here.
	/// </summary>
	public static class Kickoff {

		/// <summary>
		/// Simple marker denoting if the loader has already went through startup procedures.
		/// </summary>
		public static bool KickoffRan {
			get;
			private set;
		} = false;

		private static void StartLoader() {
			if ( KickoffRan ) {
				ExtendedLogger.Message( "Already ran through startup, skipping." );

				return;
			}

			PrintInfo();

			ExtendedLogger.Message( "Welcome to Kyu Vulpes Mod Loader for Hunt and Snare!" );

			//ModLoader.BeginModLoading();

			ExtendedLogger.Message( $"Current scene is:\t{SceneManager.GetActiveScene().name}" );

			SceneManager.sceneLoaded += ( scene, loadMode ) => ExtendedLogger.Message( $"Scene `{scene.name}` (index: {scene.buildIndex}) was loaded using `{Enum.GetName( typeof( LoadSceneMode ), loadMode )}`." );
			SceneManager.sceneUnloaded += ( scene ) => ExtendedLogger.Message( $"Scene `{scene.name}` was unloaded." );

			ModLoader.BeginModLoading();

			KickoffRan = true;
		}

		[Conditional( "DEBUG" )]
		private static void PrintInfo() {
			var builder = new StringBuilder().Append( "Stacktrace:\n" )
				.AppendLine( new StackTrace( 1 ).ToString() ).Append( "Unity Runtime Information:\n" )
				.Append( "Unity Engine Version:\t" ).AppendLine( Application.unityVersion )
				.Append( "Graphics API:\t" ).AppendLine( Enum.GetName( typeof( GraphicsDeviceType ), SystemInfo.graphicsDeviceType ) )
				.Append( "GPU:\t" ).AppendLine( SystemInfo.graphicsDeviceName )
				.Append( "GPU Driver:\t" ).AppendLine( SystemInfo.graphicsDeviceVersion )
				.Append( "Device:\t" ).AppendLine( SystemInfo.deviceName )
				.Append( "Operating System:\t" ).AppendLine( SystemInfo.operatingSystem )
				.Append( "OS Family:\t" ).AppendLine( Enum.GetName( typeof( OperatingSystemFamily ), SystemInfo.operatingSystemFamily ) )
				.Append( "Processor Type:\t" ).AppendLine( SystemInfo.processorType )
				.Append( "Processor Count:\t" ).AppendLine( SystemInfo.processorCount.ToString() )
				.AppendLine( SystemInfo.batteryStatus == BatteryStatus.Unknown ? "Unknown Battery Status." : "Has Battery." )
				.AppendLine( "Loaded Assemblies at Mod Loader Start:" );

			foreach ( var asm in AppDomain.CurrentDomain.GetAssemblies() ) {
				// $"Assembly `{asm.FullName}` was loaded."
				builder.AppendLine( asm.FullName );
			}

			ExtendedLogger.Message( builder.ToString().Trim() );
		}

	}
}
