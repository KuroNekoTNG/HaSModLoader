# Kyu Vulpes' Hunt and Snare Mod Loader

Welcome friend to my mod loader. This took a few days to make and only works on Windows. If you are a programmer yourself, please feel free to expand it to work on Linux.
This mod was made with annoyance because the quest for Simmae wasn't working for me, so I decided to try and get it working and ended up making a mod loader, whoops.
Either way, there isn't much to this mod loader and as such, only 1 page is needed.

# Getting your mod to load.

You will need to structure your mod folder like that of RimWorlds.
There needs to be a file called `info.json` and who's contents looks like:
> [!div class="tabbedCodeSnippets" data-resources="OutlookServices.Calendar"]
> ```json
> {
> 	"name": "Example Mod",
> 	"desc": "This is an example mod.",
> 	"author": "Kyu Vulpes",
> 	"version": {
> 		"major": 1,
> 		"minor": 0,
> 		"patch": 0
> 	},
> 	"packageID": "3ed42d4e-30c9-4043-9410-9132a29ad9e6"
> }
> ```
If you are using Visual Studio, you can open the `C# Interactive` window and type:
> [!div class="tabbedCodeSnippets" data-resources="OutlookServices.Calendar"]
> ```cs
> Guid.NewGuid()
> ```
and it will print out one for you. Guids might be used later for more things, but for now they are used to keep settings seperated from each mod.
There is also the optional `hash.sha` and `hash.md5` file. These files are used to ensure data integraty and that no tampering has been done.
SHA256 takes precidence over MD5 as SHA is much better than MD5. The file must be laid out to where the hash (left) is paired with a realitive path (right) seperated by a tab (`\t`).
```
ce953c46329399327a3c7e94a7892df6d44f4600b7c1694e82c502a877385871	./Audio/Music/Example Music.mp3
```
Once you have the `info.json` file (again, the hash file is optional) you can start working on your mod.

## Coding

There are going to be 3 main classes from this library. They are the

* KyuVulpes.HaSModLoader.Loader.StaticContructorOnLoadAttribute
* KyuVulpes.HaSModLoader.Core.ExtendedLogger
* KyuVulpes.HaSModLoader.Core.Mod

The `ModInfo` object might be of use when wanting to grab things from the mod folder. Let me break down what each one does/is for.

### StaticContructorOnLoadAttribute

This attribute is used to load constructors from the main thread, simple as that.
So imagine you have an image that you want to load into Unity, you cannot do that on any other thread.
You might think, "Well, I'll just load it using the Mod's constructor!" And you can, but that won't look nice when calling things.
Best to seperate that out into other classes that make sense, hence this attribute.

### ExtendedLogger

This is a logging class that will print what you send to it to Unity, but also opens up a console and will print to that as well.
This is useful when doing something like debugging as you can just print those messages out.
It will even color code them to match their level.

* Error: Red
* Warning: Yellow
* Normal: White

This makes it easy to know what is happening.

### Mod

This is the most needed classes of them all, as without it, you will not be able to do a lot of things.
There is no way to work around this, as the loader will check every assembly it comes across and looks for any class that extends `Mod`.

## Settings

Once you have made a simple object that extends `Mod`, you can call into it to save any settings you wish.
Settings are stored in a binary format, and as such do make it harder to modify outside of running the game/mod.
Note that you are restricted in what you can store in. Settings is restricted to primitives, enums, Vector[4/3/3Ints/2/2Ints], and List<T>s where T is everything mentioned before.
