# What is this?

This is a modification loader for the adult furry game [Hunt and Snare](https://store.steampowered.com/app/944330/Hunt_and_Snare/).
This was thought up while I was bored and the quest with Sinnae was irritating me as I could not find the clip board.
All that turned into this mod loader for the game.

# Is the game decompiled here?

No, and that would be illegal. Instead, this mod loader uses a library called the [UnityAssemblyInjector](https://github.com/avail/UnityAssemblyInjector) to inject the `HaS_Mod_Loader.dll` into the game.
And because this is using .NET, it means that you can easily get method names, classes, etc from the `Assembly-CSharp.dll` and any other .NET DLL out there.
This mod loader is only open-source and can be licensed under the GNU GPL because it uses no code from the game.

# How do I install this?

You can download the provided zip archive file under the releases as it contains everything needed to install.
Just extract it into the Data path for the game.

> Windows: C:\Program Files (x86)\Steam\steamapps\common\Hunt and Snare

The UnityAssemblyInjector is, of course, a Windows-Only library. As such, the non-damaging version of the mod cannot run on Linux or macOS.
An alternitive would be to try and use the Patcher for non-Windows builds. You will need Mono installed to run the patcher as it is made using .NET Framework and not .NET Core.
The patcher works by patching the `UnityEngine.CoreModule.dll` file and tells it to call the method inside the HaS_Mod_Loader.dll assembly to kick things off.
You will need Mono to run this as the WINE-Mono combo will not work from what I (Kyu Vulpes) has tested.
To run the patcher, open up a terminal and go to where you downloaded the patcher.
Once there you will also need to know where the game is stored.

For Steam: Go to Library -> Right Click `Hunt and Snare` -> Manage -> Browse Local Files.

Then copy the path. Next, type the following command, replace [Path to Hunt and Snare] with the path you just copied into it:
```
mono "HaS Patcher.exe" --game-path "[Path to Hunt and Snare]"
```
Once the game has been patched, you can now use any supported mods. Just remember that if the game updates or you repair the game, you will have to rerun the patcher.
Which is why, while the patcher will work on Windows as well, that it is not adivsable as you can go with the non-destructive version.

# Is there documentation to this?

Yes, however, at the time being, you will need to download [DocFX](https://github.com/dotnet/docfx/releases).
This is because DocFX is used to make the documentation instead of using the Wiki page of GitLab.
Simply extract it, be sure to add it to the path variable, and run it like:
```
docfx serve HuntAndSnareMod/_site
```
This should start a local web server on port 8080.

You can also go [here](https://kuronekotng.gitlab.io/HaSModLoader/) to access the docs.