﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

using Mono.Cecil;
using Mono.Cecil.Cil;

using MethodAttributes = Mono.Cecil.MethodAttributes;

namespace HaSPatcher {
	internal static class Program {

		public static void Main( string[] args ) {

#if DEBUG
			if ( !System.Diagnostics.Debugger.IsAttached ) {
				System.Diagnostics.Debugger.Launch();
			} else {
				System.Diagnostics.Debugger.Break();
			}
#endif

			if ( args[0] == "--game-path" ) {
				Patcher( args[1] );

				return;
			}

			if ( Assembly.GetExecutingAssembly().Location.Contains( "/Hunt and Snare/" ) ) {
				Patcher( Assembly.GetExecutingAssembly().Location );

				return;
			}

			Console.WriteLine( "Failed to patch, no path provided or not in game dir." );
		}

		private static void Patcher( string path ) {
			var managePath = Path.Combine( path, "ruffleneck_Data", "Managed" );
			var coreModPath = Path.Combine( managePath, "UnityEngine.CoreModule.dll" );

			if ( !File.Exists( coreModPath ) ) {
				throw new FileNotFoundException( "Please point the patcher at the correct directory." );
			}

			File.Copy( "./HaS_Mod_Loader.dll", Path.Combine( managePath, "HaS_Mod_Loader.dll" ), true );

			var defaultResolver = new DefaultAssemblyResolver();

			defaultResolver.AddSearchDirectory( managePath );

			var readerParams = new ReaderParameters() {
				AssemblyResolver = defaultResolver
			};

			var asmStream = new FileStream( coreModPath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read );

			var unityDef = AssemblyDefinition.ReadAssembly( asmStream, readerParams );

			if ( !IsAlreadyInjected() ) {
				Console.WriteLine( "Mod Loader already injected." );

				asmStream.Close();

				return;
			}

			using ( unityDef ) {
				using ( var injected = AssemblyDefinition.ReadAssembly( "HaS_Mod_Loader.dll", readerParams ) ) {
					var ogInjectMethod = injected.MainModule.Types.First( t => t.Name == "Kickoff" )
						.Methods.First( m => m.Name == "StartLoader" );
					var injectMeth = unityDef.MainModule.ImportReference( ogInjectMethod );
					var application = unityDef.MainModule.Types.First( t => t.Name == "Application" );
					var voidType = unityDef.MainModule.ImportReference( typeof( void ) );
					var classCon = new MethodDefinition(
						".cctor",
						MethodAttributes.Static | MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName,
						voidType
					);
					var ilProc = classCon.Body.GetILProcessor();

					ilProc.Append( ilProc.Create( OpCodes.Call, injectMeth ) );
					ilProc.Append( ilProc.Create( OpCodes.Ret ) );

					application.Methods.Add( classCon );

					unityDef.Write();
				}
			}

			asmStream.Close();

			bool IsAlreadyInjected() {
				if ( unityDef.MainModule.AssemblyReferences.Any( a => a.Name.Contains( "HaS_Mod_Loader" ) ) ) {
					return false;
				}

				return true;
			}
		}

	}
}
