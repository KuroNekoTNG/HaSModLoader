﻿using System.Collections.Generic;

using HarmonyLib;

using KyuVulpes.HaSModLoader.Core;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace SampleMod.HarmonyPatching {

	[HarmonyPatch( typeof( Actor ) )]
	public static class ActorPatching {

		public static readonly (string name, Vector3 pos)[] LOCATIONS = new[] {
			( "Ship Camp Site", new Vector3( 565.0f, 8.5f, 978.1f ) ),
			( "Ship", new Vector3( 586.0f, 9.5f, 1121.4f ) ),
			( "Hottie", new Vector3( 428.2f, 13.7f, 1228.6f ) ),
			( "Moutain Passage", new Vector3( -489.0f, 22.6f, -610.1f ) ),
			( "Amber Woods", new Vector3( 746.0f, 12.1f, -1915.9f ) ),
			( "Traders?", new Vector3( 1231.1f, 23.9f, -1221.0f ) ),
			( "Save Location", new Vector3( 532.9f, 7.2f, 1104.3f ) )
		};

		private static Dictionary<GameObject, Vector3> actorPos = new Dictionary<GameObject, Vector3>();

		static ActorPatching() {
			SceneManager.activeSceneChanged += ( newScene, oldScene ) => actorPos.Clear();
		}

		[HarmonyPrefix]
		[HarmonyPatch( "Update", MethodType.Normal )]
		private static void Update( Actor __instance ) {
			var actTrans = __instance.gameObject.transform;

			if ( actorPos.ContainsKey( __instance.gameObject ) ) {
				if ( actorPos[__instance.gameObject] != actTrans.position ) {
					actorPos[__instance.gameObject] = actTrans.position;

					ExtendedLogger.Message( $"Actor `{__instance.gameObject.name}` is located at `{actTrans.position}`." );
				}
			} else {
				actorPos.Add( __instance.gameObject, actTrans.position );

				ExtendedLogger.Message( $"Actor `{__instance.gameObject.name}` is located at `{actTrans.position}`." );
			}

			if ( !__instance.isPlayer ) {
				return;
			}

			(string name, Vector3 pos)? tuple = null;

			if ( Input.GetKeyDown( KeyCode.F1 ) ) {
				tuple = LOCATIONS[0];
			} else if ( Input.GetKeyDown( KeyCode.F2 ) ) {
				tuple = LOCATIONS[1];
			} else if ( Input.GetKeyDown( KeyCode.F3 ) ) {
				tuple = LOCATIONS[6];
			} else if ( Input.GetKeyDown( KeyCode.F4 ) ) {
				tuple = LOCATIONS[2];
			} else if ( Input.GetKeyDown( KeyCode.F5 ) ) {
				tuple = LOCATIONS[5];
			} else if ( Input.GetKeyDown( KeyCode.F6 ) ) {
				tuple = LOCATIONS[3];
			} else if ( Input.GetKeyDown( KeyCode.F7 ) ) {
				tuple = LOCATIONS[4];
			}

			if ( tuple is null ) {
				return;
			}

			ExtendedLogger.Message( $"Teleporting to `{tuple?.name}` at `{tuple?.pos}`." );

			if ( Actor.Warp( __instance, ( Vector3 )tuple?.pos ) ) {
				ExtendedLogger.Message( "Successfully warped to location." );
			} else {
				ExtendedLogger.Error( "Failed to warp to location." );
			}
		}
	}
}
