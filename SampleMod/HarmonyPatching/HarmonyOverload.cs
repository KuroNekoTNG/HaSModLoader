﻿using System;

using HarmonyLib;

using KyuVulpes.HaSModLoader.Core;
using KyuVulpes.HaSModLoader.Loader;

namespace SampleMod.HarmonyPatching {
	[StaticConstructorOnLoad]
	internal static class HarmonyOverload {

		internal static Harmony harmonyInst;

		static HarmonyOverload() {
			ExtendedLogger.Message( "Starting Harmony for `SampleMod`." );

			try {
				Harmony.DEBUG = true;
				harmonyInst = new Harmony( "com.kyu.vulpes.sample" );
				harmonyInst.PatchAll();
			} catch ( Exception e ) {
				ExtendedLogger.Error( e );
			}
		}

	}
}
