﻿using KyuVulpes.HaSModLoader.Core;

namespace SampleMod {
	public class SampleMod : Mod {

		public SampleMod( ModInfo info ) : base( info ) {
			ExtendedLogger.Message( $"Hello from `{GetType().FullName}` constructor!" );
		}
	}
}
